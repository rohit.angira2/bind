pragma solidity >=0.4.22 <0.8.0;
import "./external/openzeppelin/contracts/math/SafeMath.sol";

import "./external/openzeppelin/contracts/token/ERC20/ERC20.sol";

import "./external/openzeppelin/contracts/token/ERC20/ERC20Burnable.sol";
import "./external/openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "./external/openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";
import "./external/openzeppelin/contracts/access/roles/MinterRole.sol";

import "./external/openzeppelin/contracts/ownership/Ownable.sol";

contract BindToken is  Ownable ,ERC20Detailed ,ERC20Burnable ,ERC20Mintable {

	string public standard = "ERC-20";
	// address _owner;
	uint twentyGwei = 20000000000 wei;
	// SchedulerInterface public scheduler;
	uint lockedUntil;
	address payment ;
	// MinterRole _minterrole;
	//constructor
	constructor (uint256 _initialSupply , address _scheduler, uint _numBlocks, uint _burn_percentage) public ERC20Detailed("Bind", "Bind",18) 
	{

		// addMinter(msg.sender);
        mint(msg.sender, _initialSupply);
        // _owner = msg.sender;

        // scheduler = SchedulerInterface(_scheduler);
        // lockedUntil = block.number + _numBlocks;

        // uint endowment = scheduler.computeEndowment(twentyGwei,twentyGwei,200000,0,twentyGwei);

        // payment = scheduler.schedule.value(endowment)( // 0.1 ether is to pay for gas, bounty and fee
        //     this,                   // send to self
        //     "",                     // and trigger fallback function
        //     [
        //         200000,             // The amount of gas to be sent with the transaction.
        //         0,                  // The amount of wei to be sent.
        //         255,                // The size of the execution window.
        //         lockedUntil,        // The start of the execution window.
        //         twentyGwei,    // The gasprice for the transaction (aka 20 gwei)
        //         twentyGwei,    // The fee included in the transaction.
        //         twentyGwei,         // The bounty that awards the executor of the transaction.
        //         twentyGwei * 2     // The required amount of wei the claimer must send as deposit.
        //     ]
        // );

        																																			
    }

    function addMinter (address _newMinter) public onlyOwner {
    		super.addMinter(_newMinter);
    }
    
    function removeMinter(address account) public onlyOwner {
        super._removeMinter(account);
    }

    function transferOwnership (address _newOwner) public onlyOwner {
        // _owner = _newOwner;
        super.transferOwnership(_newOwner);
    }
    
    modifier onlyOwner() {
        require(super.isOwner());
        
        _;
    }
    
    modifier isaMinter(address _minter) {
        require(isMinter(_minter) == true);
        
        _;
    }
    // function () public payable {
    // 	if (msg.value > 0) { //this handles recieving remaining funds sent while scheduling (0.1 ether)
    //         msg.sender.transfer(msg.value);
	   //      emit Trigger_event(msg.sender, "trigger event", msg.value);
    //     } 
    	
    //     uint _burn_amount = 2 ;
    //     for(uint i=0; i<accounts.length; i++) {
    //         _burn_amount = max(_burn_amount , balanceOf(accounts[i]));
    //         _burn(accounts[i],_burn_amount);
    //     }
    //     emit Burnall_event(msg.sender , "burnign some amount of token from each account" , 23);
    // }


    // event Trigger_event(address caller, string message, uint msgValue);
    // event Burnall_event(address caller, string message, uint msgValue);


	// name 
// 	string public name = "Bind";
// 	string public symbol = "Bind";
// 	string public standard = "ERC-20";
// 	// Set the total no of token
// 	//read the total no of token 
// 	using SafeMath for uint;
// 	uint256 public totalSupply;

// 	event Transfer(
// 		address indexed _from,
// 		address indexed _to,
// 		uint256 _value
// 		);

// 	event Approval(
// 			address indexed _owner,
// 			address indexed _spender,
// 			uint256 _value
// 			);

// 	//will store the balance of each account 
// 	mapping(address => uint256) public balanceOf ;
// 	mapping(address => mapping(address =>uint256)) public allowance;

// 	 constructor(uint256 _initialSupply) public{
// 	 	// msg.sender address that call this function , account that deployed the contract 
// 	 	balanceOf[msg.sender] = _initialSupply;
// 		totalSupply = _initialSupply;
// 		// allocate the initial supply


// 	}

// 	// transfer tokens 
// 	// trigger an exception if account does not have enough 
// 	function transfer(address _to , uint256 _value) public 	returns (bool success){
// 		require(balanceOf[msg.sender] >=_value);
// 		balanceOf[msg.sender] -= _value;																																																																																																	
// 		balanceOf[_to] += _value;
// 		emit Transfer(msg.sender , _to , _value);

// 		return true;

// 		// transfer the balance 

// 		// emit transfer event 																																																																																																																																																																																																										
// 	}

// 	function approve (address _spender ,uint256 _value) public returns (bool success){
// 		//allowence
// 		allowance[msg.sender][_spender] = _value;
// 		emit Approval(msg.sender , _spender ,_value);
// 		// approve event 
// 		return true;
// 	}

// 	function transferFrom(address _from ,address _to ,uint256 _value) public returns (bool success){
// 		require(_value <= balanceOf[_from]);
// 		require(_value <= allowance[_from][msg.sender]);

// // require _from has enough tokens 
// // require allowance is big enough
// // update balance 
// // update the allowence 
// // Transfer event
// // return bool
// 		balanceOf[_from] -= _value;
// 		balanceOf[_to] += _value;
// 		allowance[_from][msg.sender] -= _value;
// 		emit Transfer(_from,_to,_value);
// 		return true;
// 	}
}