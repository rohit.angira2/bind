var BindToken = artifacts.require("./BindToken.sol");

contract('BindToken', function(accounts) {
  var tokenInstance;

  it('initializes the contract with the correct values', function() {
    return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
      tokenInstance = instance;
      return tokenInstance.name();
    }).then(function(name) {
      assert.equal(name, 'Bind', 'has the correct name');
      return tokenInstance.symbol();
    }).then(function(symbol) {
      assert.equal(symbol, 'Bind', 'has the correct symbol');
      return tokenInstance.standard();
    }).then(function(standard) {
      assert.equal(standard, 'ERC-20', 'has the correct standard');
      return tokenInstance.decimals();
    }).then(function(value){
      assert.equal(value,18 , "has the correct dacimals");
      return tokenInstance.balanceOf(accounts[0]);
    }).then(function(value){
      assert.equal(value,3333333 , "has the correct balance");
      return tokenInstance.totalSupply();
    }).then(function(value){
      assert.equal(value,3333333,"has correct total supply");
    });
  })

  
  it('transfer 10 token to accoutn[1]',function(){
     return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
      tokenInstance = instance;
      return tokenInstance.transfer(accounts[1],10);
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
      return tokenInstance.balanceOf(accounts[1]); 
    }).then(function(value){
      assert.equal(value,10,"successfully transfereed toekns");
    });
  })

  it('burn some token from account[1]',function(){
     return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
      tokenInstance = instance;
      return tokenInstance.transfer(accounts[1],10);
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
      return tokenInstance.increaseAllowance(accounts[0],50,{from:accounts[1]})    
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'Approval', 'should be the "Approval" event');
      return tokenInstance.burnFrom(accounts[1],2);
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 2, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
      return tokenInstance.balanceOf(accounts[1]); 
    }).then(function(value){
      assert.equal(value.toNumber(),18,"successfully transfereed toekns");
    });
  })

  it('add a minter', function() {
    return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
      tokenInstance = instance;
      return tokenInstance.addMinter(accounts[2]);
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'MinterAdded', 'should be the "MinterAdded" event');
      return tokenInstance.isMinter(accounts[2]);
      // return tokenInstance;
      // assert.equal(tokenInstance.isMinter(accounts[1].address) , true ,'adds a minter');   
    }).then(function(value){
      assert.equal(value, true , "checked minter MinterAdded");
    });
  })


 // it('remove a minter', function() {
 //    return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
 //      tokenInstance = instance;
 //      // tokenInstance.addMinter(accounts[1]);
 //      return tokenInstance.renounceMinter({from:accounts[1]});
 //    }).then(function(receipt){
 //      assert.equal(receipt.logs.length, 1, 'triggers one event');
 //      assert.equal(receipt.logs[0].event, 'MinterRemoved', 'should be the "MinterRemoved" event');
 //      return tokenInstance.isMinter(accounts[1]);
 //      // return tokenInstance;
 //      // assert.equal(tokenInstance.isMinter(accounts[1].address) , true ,'adds a minter');   
 //    }).then(function(value){
 //      assert.equal(value, false , "checked MinterRemoved MinterRemoved");
 //    });
 //  })

 it('remove a minter using removeMinter', function() {
    return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
      tokenInstance = instance;
      tokenInstance.addMinter(accounts[1]);
      return tokenInstance.removeMinter(accounts[1]);
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'MinterRemoved', 'should be the "MinterRemoved" event');
      return tokenInstance.isMinter(accounts[1]);
      // return tokenInstance;
      // assert.equal(tokenInstance.isMinter(accounts[1].address) , true ,'adds a minter');   
    }).then(function(value){
      assert.equal(value, false , "checked MinterRemoved MinterRemoved");
    });
  })

 it('tansfer the ownership to other address', function() {
    return BindToken.deployed(3333333,accounts[0],3,2).then(function(instance) {
      tokenInstance = instance;
      return tokenInstance.transferOwnership(accounts[1])   
    
    }).then(function(receipt){
      assert.equal(receipt.logs.length, 1, 'triggers one event');
      assert.equal(receipt.logs[0].event, 'OwnershipTransferred', 'should be the "OwnershipTransferred" event');
      return tokenInstance.owner();
    }).then(function(newowner) {
      assert.equal(newowner,accounts[1],'cahnged the ownership');
    });
  })

  // it('allocates the initial supply upon deployment', function() {
  //   return BindToken.deployed().then(function(instance) {
  //     tokenInstance = instance;
  //     return tokenInstance.totalSupply();
  //   }).then(function(totalSupply) {
  //     assert.equal(totalSupply.toNumber(), 3333333, 'sets the total supply to 1,000,000');
  //     return tokenInstance.balanceOf(accounts[0]);
  //   }).then(function(adminBalance) {
  //     assert.equal(adminBalance.toNumber(), 3333333, 'it allocates the initial supply to the admin account');
  //   });
  // });

  // it('all accounts except 0 hold value 0', function() {
  //   return BindToken.deployed().then(function(instance) {
  //     tokenInstance = instance;
  //     var chk = true;
  //     for(var i =0 ; i< accounts.length ;i++){
  //       if (i != 0){
  //         if (tokenInstance.balanceOf(accounts[i]) > 0){
  //           chk = false;
  //           break;
  //         }
  //       }
  //     }
  //     return chk;
  //   }).then(function(i) {
  //     assert.equal(i, true, 'all accounts except 0 holds o tokens');
      
  //   });
  // });

  // it('all accounts hold tokens', function() {
  //   return BindToken.deployed().then(function(instance) {

  //     tokenInstance = instance;
      
  //     for(var i=1; i<accounts.length; i++){
  //       tokenInstance.transfer(accounts[i], 4, { from: accounts[0] });
  //       }

  //     var chk = true;
  //     for(var i =0 ; i< accounts.length ;i++){
        
  //         if (tokenInstance.balanceOf(accounts[i])  <= 0 ){
  //           chk = false;
  //           break;
          
  //       }
  //     }
  //     return chk;
  //   }).then(function(i) {
  //     assert.equal(i, true, 'all accounts hold some value');
      
  //   });
  // });


  // it("checks for burn from every account",function(){
  //   return BindToken.deployed().then(function(instance){
  //     tokenInstance = instance;
  //     // first give t4 tokens to each except 0
  //     for(var i=1; i<accounts.length; i++){
  //       tokenInstance.transfer(accounts[i], 4, { from: accounts[0] });
  //       }

  //   });
  // });

  // it('transfers token ownership', function() {
  //   return BindToken.deployed().then(function(instance) {
  //     tokenInstance = instance;
  //     // Test `require` statement first by transferring something larger than the sender's balance
  //     return tokenInstance.transfer.call(accounts[1], 99999999);
  //   }).then(assert.fail).catch(function(error) {
  //     assert(error.message.indexOf('revert') >= 0, 'error message must contain revert');
  //     return tokenInstance.transfer.call(accounts[1], 250	, { from: accounts[0] });
  //   }).then(function(success) {
  //     assert.equal(success, true, 'it returns true');
  //     return tokenInstance.transfer(accounts[1], 250, { from: accounts[0] });
  //   }).then(function(receipt) {
  //     assert.equal(receipt.logs.length, 1, 'triggers one event');
  //     assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
  //     assert.equal(receipt.logs[0].args._from, accounts[0], 'logs the account the tokens are transferred from');
  //     assert.equal(receipt.logs[0].args._to, accounts[1], 'logs the account the tokens are transferred to');
  //     assert.equal(receipt.logs[0].args._value, 250, 'logs the transfer amount');
  //     return tokenInstance.balanceOf(accounts[1]);
  //   }).then(function(balance) {
  //     assert.equal(balance.toNumber(), 250, 'adds the amount to the receiving account');
  //     return tokenInstance.balanceOf(accounts[0]);
  //   }).then(function(balance) {
  //     assert.equal(balance.toNumber(), 3333083, 'deducts the amount from the sending account');
  //   });
  // });

  // it('approves tokens for delegated transfer', function() {
  //   return BindToken.deployed().then(function(instance) {
  //     tokenInstance = instance;
  //     return tokenInstance.approve.call(accounts[1], 100);
  //   }).then(function(success) {
  //     assert.equal(success, true, 'it returns true');
  //     return tokenInstance.approve(accounts[1], 100, { from: accounts[0] });
  //   }).then(function(receipt) {
  //     assert.equal(receipt.logs.length, 1, 'triggers one event');
  //     assert.equal(receipt.logs[0].event, 'Approval', 'should be the "Approval" event');
  //     assert.equal(receipt.logs[0].args._owner, accounts[0], 'logs the account the tokens are authorized by');
  //     assert.equal(receipt.logs[0].args._spender, accounts[1], 'logs the account the tokens are authorized to');
  //     assert.equal(receipt.logs[0].args._value, 100, 'logs the transfer amount');
  //     return tokenInstance.allowance(accounts[0], accounts[1]);
  //   }).then(function(allowance) {
  //     assert.equal(allowance.toNumber(), 100, 'stores the allowance for delegated trasnfer');
  //   });
  // });

  // it('handles delegated token transfers', function() {
  //   return BindToken.deployed().then(function(instance) {
  //     tokenInstance = instance;
  //     fromAccount = accounts[2];
  //     toAccount = accounts[3];
  //     spendingAccount = accounts[4];
  //     // Transfer some tokens to fromAccount
  //     return tokenInstance.transfer(fromAccount, 100, { from: accounts[0] });
  //   }).then(function(receipt) {
  //     // Approve spendingAccount to spend 10 tokens form fromAccount
  //     return tokenInstance.approve(spendingAccount, 10, { from: fromAccount });
  //   }).then(function(receipt) {
  //     // Try transferring something larger than the sender's balance
  //     return tokenInstance.transferFrom(fromAccount, toAccount, 9999, { from: spendingAccount });
  //   }).then(assert.fail).catch(function(error) {
  //     assert(error.message.indexOf('revert') >= 0, 'cannot transfer value larger than balance');
  //     // Try transferring something larger than the approved amount
  //     return tokenInstance.transferFrom(fromAccount, toAccount, 20, { from: spendingAccount });
  //   }).then(assert.fail).catch(function(error) {
  //     assert(error.message.indexOf('revert') >= 0, 'cannot transfer value larger than approved amount');
  //     return tokenInstance.transferFrom.call(fromAccount, toAccount, 10, { from: spendingAccount });
  //   }).then(function(success) {
  //     assert.equal(success, true);
  //     return tokenInstance.transferFrom(fromAccount, toAccount, 10, { from: spendingAccount });
  //   }).then(function(receipt) {
  //     assert.equal(receipt.logs.length, 1, 'triggers one event');
  //     assert.equal(receipt.logs[0].event, 'Transfer', 'should be the "Transfer" event');
  //     assert.equal(receipt.logs[0].args._from, fromAccount, 'logs the account the tokens are transferred from');
  //     assert.equal(receipt.logs[0].args._to, toAccount, 'logs the account the tokens are transferred to');
  //     assert.equal(receipt.logs[0].args._value, 10, 'logs the transfer amount');
  //     return tokenInstance.balanceOf(fromAccount);
  //   }).then(function(balance) {
  //     assert.equal(balance.toNumber(), 90, 'deducts the amount from the sending account');
  //     return tokenInstance.balanceOf(toAccount);
  //   }).then(function(balance) {
  //     assert.equal(balance.toNumber(), 10, 'adds the amount from the receiving account');
  //     return tokenInstance.allowance(fromAccount, spendingAccount);
  //   }).then(function(allowance) {
  //     assert.equal(allowance.toNumber(), 0, 'deducts the amount from the allowance');
  //   });
  // });
});